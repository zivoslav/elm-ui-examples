## Video Library

### Features
- [x] Folder / File like structure to store videos
- [x] Play (mp4) videos with Html5 video player
- [x] URL Navigation
- [ ] Restfully create entities
- [ ] Fuzzy search in the library view

### Issues
- [x] URL parameters cannot be unset https://github.com/sporto/hop/issues/3
- [ ] Initial state from history is not working https://github.com/elm-community/elm-history/issues/3.
